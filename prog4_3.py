# Driver program import functions/classes from part 1 and 2
# This has a main function specificaiton and read the first command line argument as a file
# Tokenize and parse all lines of file

import sys
import prog4_1
import prog4_2


def main():
    print("Assignment #4-3, Travis Dattilo, travisdattilo@yahoo.com")

    firstCmdArg = sys.argv[1]
    sm = prog4_2.StackMachine()

    toHold = []
    toPrint = []
    lineCount = 0
    

    with open(firstCmdArg) as f:
        content = f.readlines()
        for line in content:
            toHold.append(prog4_1.Tokenize(line))
            lineCount += 1 

    for i in toHold:
        prog4_1.Parse(i)

    try:
        executeLineCount = 1
        while(sm._CurrentLine < lineCount):
            if(sm._CurrentLine < 0):
                raise ValueError
            x = sm.Execute(toHold[sm._CurrentLine])
            if(x != None):
                toPrint.append(x)
            executeLineCount += 1

        for i in range(0,len(toPrint)):
            print(toPrint[i])

        print("Program terminated correctly")
    except IndexError as exc:        
        num = repr(executeLineCount)   
        badTokens = ' '.join(toHold[sm._CurrentLine])
        raise IndexError("Line " + num + ": " + badTokens + " caused Invalid Memory Access") from exc
    except ValueError as exc:
        num = repr(executeLineCount)
        print("Trying to execute invalid line: " + num)   

main()


































