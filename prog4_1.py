def Tokenize(string):
    _data = []     # data structure
    
    _validTokens = ["push","pop","add","sub","mul","div","mod","skip","save","get"]
    def isNumber(s):
        try:
            int(s)
            return True 
        except ValueError:
            return False
    def isValid(s):
            validity = 0
            for goodToken in _validTokens:
                if(s == goodToken or isNumber(s)):
                    validity = 1
            return validity == 1
        
    sp = string.split()                # splits the string on whitespace
    for i in range (0,len(sp)):        # for each token in (split) sp from the space character
        currentToken = sp[i]
        if(isValid(currentToken) != True):
            raise ValueError("Unexpected token: " + currentToken)
        _data.insert(i,currentToken)
    return _data
    
def Parse(toParse):
    def isNumber(s):
        try:
            int(s)
            return True 
        except ValueError:
            return False
            
    def isSingleCmd(s):
        validity = 0
        _validSingleCmd = ["pop","add","sub","mul","div","mod","skip"]
        for i in _validSingleCmd:
            if (s == i):
                validity = 1
        return validity == 1

    def isDoubleCmd(s):
        validity = 0
        _validDoubleCmd = ["push","save","get"]
        for i in _validDoubleCmd:
            if (s == i):
                validity = 1
        return validity == 1

    validity = None
    
    if (len(toParse) == 2 ):
        validity = isDoubleCmd(toParse[0]) and isNumber(toParse[1])
    elif(len(toParse) == 1):
        validity = isSingleCmd(toParse[0])
    
    if(validity == True):
        return True     
    else:
        badInput = ' '.join(toParse)
        raise ValueError("Parse error: " + badInput) 

      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
