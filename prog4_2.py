class StackMachine:
    def __init__(self):
        self.items = []
        self._CurrentLine = 0
        self.mem = {}
    def isEmpty(self,tokenSet):     
        if not tokenSet:
            return True
        else:
            return False       

    def Execute(self,tokens):           # tokens is a list                 
        if (tokens[0] == "push"):
            self.items.append(tokens[1])   
        elif(tokens[0] == "pop"):
            if self.isEmpty(self.items):      
                raise IndexError("Invalid Memory Access") 
            else:
                x = self.items[-1]
                self.items.pop()
                self._CurrentLine += 1  # since there is a return we must increment CurrentLine here
                return int(x)
        elif (tokens[0] == "add"):
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                numToAdd1 = (self.items.pop())
                numToAdd2 = (self.items.pop())
                added = int(numToAdd1) + int(numToAdd2)
                self.items.append(added)
        elif (tokens[0] == "sub"):
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                numToSub1 = (self.items.pop())
                numToSub2 = (self.items.pop())
                diff = int(numToSub1) - int(numToSub2)
                self.items.append(diff)        
        elif (tokens[0] == "mul"):
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                numToMul1 = (self.items.pop())
                numToMul2 = (self.items.pop())
                product = int(numToMul1) * int(numToMul2)
                self.items.append(product)
        elif (tokens[0] == "div"):
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                numToDiv1 = (self.items.pop())
                numToDiv2 = (self.items.pop())
                quotient = int(numToDiv1) / int(numToDiv2)
                self.items.append(quotient)
        elif (tokens[0] == "mod"):
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                numToMod1 = (self.items.pop())
                numToMod2 = (self.items.pop())
                modded = int(numToMod1) % int(numToMod2)
                self.items.append(modded) 
        elif (tokens[0] == "skip"):  
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                numToSkip1 = int(self.items.pop())
                numToSkip2 = int(self.items.pop())
                if(numToSkip1 == 0):
                    # change currentLine by numToSkip2    
                    self._CurrentLine += int(numToSkip2)   
        elif (tokens[0] == "save"):      
            if self.isEmpty(self.items):  
                raise IndexError("Invalid Memory Access") 
            else:
                toSave = self.items.pop()                         
                self.mem.update({int(tokens[1]):toSave})
        elif (tokens[0] == "get"):      
            self.items.append(self.mem.get(int(tokens[1])))
            
        self._CurrentLine += 1
             




















