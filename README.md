Travis Dattilo  
travisdattilo@yahoo.com  

"prog4_1.py"  
This python program implements the Tokenize and Parse functions. The Tokenize function
takes a string parameter that splits it into tokens on the space delimeter and checks
for semantic validity of each token. In other words, it checks that each token is one of
the following: push, pop, add, sub, mul, div, mod, skip, save, get, or a integer number.
It returns the tokens indexed in a list. The Parse function takes a list parameter, and
checks for grammatical correctness meaning push, save, and get are the only valid tokens
to be accompanied by an integer number next to them while pop, add, sub, mul, div, mod, 
and skip are left alone. If the input does not meet the validity checks in either the
Tokenize or Parse functions, appropriate errors are raised.

"prog4_2.py"  
This python program has the StackMachine class that has the "stack" container(s) to be used 
for performing the operations of push, pop, add, sub, mul, div, mod, skip, save, and get. 
This class also has a current line property, initially zero, to be used in a driver to track 
progress through a file. The Execute function is in this class and takes the implicit self 
parameter as well as a list of tokens. A series of if and else if tests check what instruction 
is trying to be performed. Push will push data onto the stack, pop will pop it off, add will 
pop two values off the stack and add them together, sub will pop two values off the stack and 
subtract the second from the first, mul will pop two values off the stack and multiply them 
together, div will pop two values from the stack and divide the second one from the first, mod 
will pop two values from the stack and mod the second from the first, skip will pop two values 
from the stack, if the first one is a zero then the current line property is increased by the 
value of the second value popped, save will pop a value from the stack and store it in the index 
specified by the integer token next to "save" in a dictionary structure, and get will retreive 
the value previously stored in the dictionary structure from the save function without removing 
from that dictionary. Everytime the execute function is called the current line property is 
incremented by 1. 

"prog4_3.py"  
This python program is a driver program using prog4_1.py and prog4_2.py and their respective
functions (described above). It takes the first command line argument that is a file to
then Tokenize, Parse, and execute the functions written (in the file). Specifically, there 
are four loops in this driver, the first tokenizes all the input in the file line by line and
indexes the token pairs or singular tokens into the list called "toHold." The second loop 
parses all the tokens in the "toHold" list. The third executes the tokenized and parsed tokens.
The last loop prints the output from the execute function. Errors are raised and printed as
helpful error messages. 

"cs320programmingrubric.pdf"  
This document is required for grading purposes.
